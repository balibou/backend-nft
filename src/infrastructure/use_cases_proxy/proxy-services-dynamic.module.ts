import { DynamicModule, Module } from '@nestjs/common';
import { AuthenticationModule } from '../../config/authentication.module';
import { AuthenticationService } from '../../config/authentication.service';
import { Signin } from '../../use_cases/signin';
import { Signup } from '../../use_cases/signup';
import { DatabaseUserRepository } from '../repositories/database-user.repository';
import { RepositoriesModule } from '../repositories/repositories.module';

@Module({
  imports: [RepositoriesModule, AuthenticationModule],
})
export class ProxyServicesDynamicModule {
  static register(): DynamicModule {
    return {
      module: ProxyServicesDynamicModule,
      providers: [
        {
          inject: [DatabaseUserRepository],
          provide: Signup,
          useFactory: (databaseUserRepository: DatabaseUserRepository) =>
            new Signup(databaseUserRepository),
        },
        {
          inject: [DatabaseUserRepository, AuthenticationService],
          provide: Signin,
          useFactory: (
            databaseUserRepository: DatabaseUserRepository,
            authenticationService: AuthenticationService,
          ) => new Signin(databaseUserRepository, authenticationService),
        },
      ],
      exports: [Signup, Signin],
    };
  }
}
