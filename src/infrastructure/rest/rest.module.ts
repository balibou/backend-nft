import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { ProxyServicesDynamicModule } from '../use_cases_proxy/proxy-services-dynamic.module';
import { UserController } from './user.controller';
import { InvalidUserErrorFilter } from './filters/invalid-user-error.filter';
import { AuthenticationModule } from '../../config/authentication.module';

@Module({
  imports: [ProxyServicesDynamicModule.register(), AuthenticationModule],
  controllers: [UserController],
  providers: [{ provide: APP_FILTER, useClass: InvalidUserErrorFilter }],
})
export class RestModule {}
