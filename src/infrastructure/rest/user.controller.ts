import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { JWTAuthGuard } from '../../config/guards/jwt-auth.guard';
import { LocalAuthGuard } from '../../config/guards/local-auth.guard';
import { Signin } from '../../use_cases/signin';
import { Signup } from '../../use_cases/signup';
import { UserEntity } from '../repositories/entities/user.entity';
import { SigninRequest } from './models/signin-request';
import { SignupRequest } from './models/signup-request';
import { GetUser } from '../decorators/user.decorator';

@Controller('/api/user')
export class UserController {
  constructor(
    private readonly signup: Signup,
    private readonly signin: Signin,
  ) {}

  @Post('/signup')
  @HttpCode(HttpStatus.CREATED)
  async postSignup(@Body() signupRequest: SignupRequest): Promise<string> {
    return this.signup.execute(signupRequest.username, signupRequest.password);
  }

  @Post('/signin')
  @UseGuards(LocalAuthGuard)
  @HttpCode(HttpStatus.OK)
  async postSignin(
    @Body() signinRequest: SigninRequest,
  ): Promise<{ accessToken: string }> {
    return {
      accessToken: await this.signin.execute(
        signinRequest.username,
        signinRequest.password,
      ),
    };
  }

  @Get('/me')
  @UseGuards(JWTAuthGuard)
  @HttpCode(HttpStatus.OK)
  me(@GetUser() user: UserEntity): UserEntity {
    return user;
  }
}
