import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from '../../config/typeorm.config';
import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import { DatabaseUserRepository } from './database-user.repository';
import { RepositoriesModule } from './repositories.module';

describe('UserRepository', () => {
  let userRepository: DatabaseUserRepository;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DatabaseUserRepository],
      imports: [
        RepositoriesModule,
        TypeOrmModule.forFeature([DatabaseUserRepository]),
        TypeOrmModule.forRoot(typeOrmConfig),
      ],
    }).compile();

    userRepository = await module.get<DatabaseUserRepository>(
      DatabaseUserRepository,
    );
  });

  beforeEach(async () => {
    await userRepository.clear();
  });

  describe('signup', () => {
    it('should create a user in the repository', async () => {
      const user = {
        username: 'user',
        password: 'password',
      };
      const newUser = await userRepository.signup(user);
      expect(newUser).toEqual('user');
    });
    it('should throw an error if user already exists', async () => {
      const user = {
        username: 'user',
        password: 'password',
      };
      await userRepository.signup(user);
      const newUser = userRepository.signup(user);

      await expect(newUser).rejects.toThrowError(
        new ConflictException('user already exists'),
      );
    });
    it('should throw an error if property is missing', async () => {
      const user = {
        username: '',
        password: 'password',
      };
      const newUser = userRepository.signup(user);

      await expect(newUser).rejects.toThrowError(
        new InternalServerErrorException(
          'username/password cannot be null or empty',
        ),
      );
    });
  });
});
