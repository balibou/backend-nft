import { UserEntity } from '../entities/user.entity';
import { Repository } from 'typeorm';
import { UserRepository } from '../../../domain/user/user.repository';
import { User } from 'src/domain/user/user';
import { ConflictException } from '@nestjs/common/exceptions/conflict.exception';

export default class DatabaseMockUserRepository
  extends Repository<UserEntity>
  implements UserRepository {
  userList = new Set();

  async signup(user: User): Promise<string> {
    if (this.userList.has(user.username)) {
      throw new ConflictException('user already exists');
    }
    this.userList.add(user.username);
    return user.username;
  }

  async validateUserPassword(user: User): Promise<string | null> {
    return user.username || null;
  }

  async validateUserUsername() {
    return;
  }
}
