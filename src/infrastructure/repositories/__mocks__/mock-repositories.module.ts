import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from '../entities/user.entity';
import DatabaseMockUserRepository from './database-mock-user.repository';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity])],
  providers: [DatabaseMockUserRepository],
  exports: [DatabaseMockUserRepository],
})
export class MockRepositoriesModule {}
