import { Module } from '@nestjs/common';
import { DatabaseUserRepository } from './database-user.repository';

@Module({
  providers: [DatabaseUserRepository],
  exports: [DatabaseUserRepository],
})
export class RepositoriesModule {}
