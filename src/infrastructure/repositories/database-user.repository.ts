import { EntityRepository, Repository } from 'typeorm';
import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import { UserRepository } from '../../domain/user/user.repository';
import { UserEntity } from './entities/user.entity';
import { User } from '../../domain/user/user';

@EntityRepository(UserEntity)
export class DatabaseUserRepository
  extends Repository<UserEntity>
  implements UserRepository {
  async signup(user: User): Promise<string> {
    const userEntity: UserEntity = await this.toUserEntity(user);

    try {
      const savedUserEntity: UserEntity = await userEntity.save();

      return this.toUser(savedUserEntity).username;
    } catch (error) {
      if (error.code === '23505') {
        throw new ConflictException('user already exists');
      } else {
        throw new InternalServerErrorException(error);
      }
    }
  }

  private toUser(userEntity: UserEntity): User {
    const user: User = new User(userEntity.username, userEntity.password);
    user.username = userEntity.username;
    user.password = userEntity.password;

    return user;
  }

  private async toUserEntity(user: User): Promise<UserEntity> {
    const userEntity: UserEntity = new UserEntity();
    userEntity.username = user.username;
    userEntity.password = user.password;

    return userEntity;
  }

  async validateUserPassword(user: User): Promise<string | null> {
    const { username, password } = user;
    const userFound = await UserEntity.findOne({ username });
    if (userFound && (await userFound.checkPassword(password))) {
      return userFound.username;
    } else {
      return null;
    }
  }

  async validateUserUsername(username: string): Promise<string | null> {
    const userFound = await UserEntity.findOne({ username });
    if (userFound) {
      return userFound.username;
    } else {
      return null;
    }
  }
}
