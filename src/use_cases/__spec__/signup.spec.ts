import { Test, TestingModule } from '@nestjs/testing';
import { Signup } from '../signup';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from '../../config/typeorm.config';
import { DatabaseUserRepository } from '../../infrastructure/repositories/database-user.repository';
import { ConflictException } from '@nestjs/common';
import { InvalidUserError } from '../../domain/user/invalid-user.error';

describe('signup', () => {
  let signup: Signup;
  let userRepository: DatabaseUserRepository;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot(typeOrmConfig),
        TypeOrmModule.forFeature([DatabaseUserRepository]),
      ],
      providers: [
        {
          inject: [DatabaseUserRepository],
          provide: Signup,
          useFactory: (databaseUserRepository: DatabaseUserRepository) =>
            new Signup(databaseUserRepository),
        },
      ],
    }).compile();

    signup = await moduleFixture.get<Signup>(Signup);
    userRepository = await moduleFixture.get(DatabaseUserRepository);
  });

  beforeEach(async () => {
    await userRepository.clear();
  });

  describe('execute', () => {
    it('should signup a user', async () => {
      expect(await signup.execute('user', 'password')).toBe('user');
    });
    it('should throw an error if username is empty', async () => {
      const newUser = signup.execute('', 'password');
      await expect(newUser).rejects.toThrowError(
        new InvalidUserError('username/password cannot be null or empty'),
      );
    });
    it('should throw an error if username is null', async () => {
      const newUser = signup.execute(null, 'password');
      await expect(newUser).rejects.toThrowError(
        new InvalidUserError('username/password cannot be null or empty'),
      );
    });
    it('should throw an error if password is empty', async () => {
      const newUser = signup.execute('user', '');
      await expect(newUser).rejects.toThrowError(
        new InvalidUserError('username/password cannot be null or empty'),
      );
    });
    it('should throw an error if pasword is null', async () => {
      const newUser = signup.execute('user', null);
      await expect(newUser).rejects.toThrowError(
        new InvalidUserError('username/password cannot be null or empty'),
      );
    });
    it('should throw an error if user already exists', async () => {
      await signup.execute('user', 'password');
      const newUser = signup.execute('user', 'password');
      await expect(newUser).rejects.toThrowError(
        new ConflictException('user already exists'),
      );
    });
  });
});
