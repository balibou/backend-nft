import { Test, TestingModule } from '@nestjs/testing';
import { Signup } from '../signup';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from '../../config/typeorm.config';
import DatabaseMockUserRepository from '../../infrastructure/repositories/__mocks__/database-mock-user.repository';
import { MockRepositoriesModule } from '../../infrastructure/repositories/__mocks__/mock-repositories.module';
import { ConflictException } from '@nestjs/common';

describe('signup useMock', () => {
  let signup: Signup;
  let databaseMockUserRepository: DatabaseMockUserRepository;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [MockRepositoriesModule, TypeOrmModule.forRoot(typeOrmConfig)],
      providers: [
        {
          inject: [DatabaseMockUserRepository],
          provide: Signup,
          useFactory: (databaseUserRepository: DatabaseMockUserRepository) =>
            new Signup(databaseUserRepository),
        },
      ],
    }).compile();

    signup = await moduleFixture.get<Signup>(Signup);
    databaseMockUserRepository = await moduleFixture.get<DatabaseMockUserRepository>(
      DatabaseMockUserRepository,
    );
  });

  beforeEach(() => {
    databaseMockUserRepository.userList.clear();
  });

  describe('execute', () => {
    it('should signup a user', async () => {
      const signedUpUser: string = await signup.execute('user', 'password');
      expect(signedUpUser).toBe('user');
    });
    it('should not signup a user', async () => {
      await signup.execute('user', 'password');
      const newUser = signup.execute('user', 'password');
      await expect(newUser).rejects.toThrow(ConflictException);
      await expect(newUser).rejects.toThrowError(
        new Error('user already exists'),
      );
    });
  });
});
