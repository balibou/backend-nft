import { Test, TestingModule } from '@nestjs/testing';
import { Signin } from '../signin';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from '../../config/typeorm.config';
import { DatabaseUserRepository } from '../../infrastructure/repositories/database-user.repository';
import { Signup } from '../signup';
import { AuthenticationModule } from '../../config/authentication.module';
import { AuthenticationService } from '../../config/authentication.service';
import { InvalidUserError } from '../../domain/user/invalid-user.error';
import { UnauthorizedException } from '@nestjs/common';

describe('signin', () => {
  let signin: Signin;
  let signup: Signup;
  let userRepository: DatabaseUserRepository;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot(typeOrmConfig),
        TypeOrmModule.forFeature([DatabaseUserRepository]),
        AuthenticationModule,
      ],
      providers: [
        {
          inject: [DatabaseUserRepository, AuthenticationService],
          provide: Signin,
          useFactory: (
            databaseUserRepository: DatabaseUserRepository,
            authenticationService: AuthenticationService,
          ) => new Signin(databaseUserRepository, authenticationService),
        },
        {
          inject: [DatabaseUserRepository],
          provide: Signup,
          useFactory: (databaseUserRepository: DatabaseUserRepository) =>
            new Signup(databaseUserRepository),
        },
      ],
    }).compile();

    signin = await moduleFixture.get<Signin>(Signin);
    signup = await moduleFixture.get<Signup>(Signup);
    userRepository = await moduleFixture.get<DatabaseUserRepository>(
      DatabaseUserRepository,
    );
  });

  beforeEach(async () => {
    await userRepository.clear();
  });

  describe('execute', () => {
    it('should signin a user', async () => {
      await signup.execute('user', 'password');
      expect(await signin.execute('user', 'password')).toEqual(
        expect.any(String),
      );
    });
    it('should throw an error when username is empty', async () => {
      await expect(signin.execute('', 'password')).rejects.toThrowError(
        new InvalidUserError('username/password cannot be null or empty'),
      );
    });
    it('should throw an error when username is null', async () => {
      await expect(signin.execute(null, 'password')).rejects.toThrowError(
        new InvalidUserError('username/password cannot be null or empty'),
      );
    });
    it('should throw an error when password is empty', async () => {
      await expect(signin.execute('user', '')).rejects.toThrowError(
        new InvalidUserError('username/password cannot be null or empty'),
      );
    });
    it('should throw an error when password is null', async () => {
      await expect(signin.execute('user', null)).rejects.toThrowError(
        new InvalidUserError('username/password cannot be null or empty'),
      );
    });
    it('should throw an error when user does not exists', async () => {
      await expect(signin.validate('user', 'password')).rejects.toThrowError(
        new UnauthorizedException('Invalid credentials'),
      );
    });
    it('should throw an error when user credentials does not match existing one', async () => {
      await signup.execute('user', 'password');
      await expect(
        signin.validate('user', 'wrongPassword'),
      ).rejects.toThrowError(new UnauthorizedException('Invalid credentials'));
    });
  });
});
