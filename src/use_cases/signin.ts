import { UserRepository } from '../domain/user/user.repository';
import { User } from '../domain/user/user';
import { UnauthorizedException } from '@nestjs/common';
import { AuthenticationService } from '../config/authentication.service';
import { JwtPayload } from '../config/interfaces/jwt-payload.interface';

export class Signin {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly authenticationService: AuthenticationService,
  ) {}

  async validate(username: string, password: string): Promise<any> {
    const newUser: User = new User(username, password);

    const foundUsername = await this.userRepository.validateUserPassword(
      newUser,
    );

    if (!foundUsername) {
      throw new UnauthorizedException('Invalid credentials');
    }

    return newUser;
  }

  async validatePayload(payload: JwtPayload): Promise<any> {
    const foundUsername = await this.userRepository.validateUserUsername(
      payload.sub,
    );

    if (!foundUsername) {
      throw new UnauthorizedException('Invalid payload');
    }

    return payload.sub;
  }

  async execute(username: string, password: string): Promise<string> {
    const newUser: User = new User(username, password);
    return this.authenticationService.signToken(newUser);
  }
}
