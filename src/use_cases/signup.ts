import { UserRepository } from '../domain/user/user.repository';
import { User } from '../domain/user/user';

export class Signup {
  constructor(private readonly userRepository: UserRepository) {}

  async execute(username: string, password: string): Promise<string> {
    const newUser: User = new User(username, password);

    return await this.userRepository.signup(newUser);
  }
}
