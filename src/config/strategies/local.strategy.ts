import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { Signin } from '../../use_cases/signin';
import { User } from '../../domain/user/user';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
  constructor(private readonly signin: Signin) {
    super({
      usernameField: 'username',
      passReqToCallback: false,
    });
  }

  validate(username: string, password: string): Promise<User> {
    return this.signin.validate(username, password);
  }
}
