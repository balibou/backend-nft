import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from '../domain/user/user';

@Injectable()
export class AuthenticationService {
  constructor(private readonly jwtService: JwtService) {}

  signToken(user: User): string {
    const payload = {
      sub: user.username,
    };

    return this.jwtService.sign(payload);
  }
}
