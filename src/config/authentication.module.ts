import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DatabaseUserRepository } from '../infrastructure/repositories/database-user.repository';
import { AuthenticationService } from './authentication.service';
import * as config from 'config';
import { JwtStrategy } from './strategies/jwt.strategy';
import { LocalStrategy } from './strategies/local.strategy';
import { Signin } from '../use_cases/signin';

const jwtConfig = config.get('jwt');

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: process.env.JWT_SECRET || jwtConfig.secret,
      signOptions: {
        expiresIn: jwtConfig.expiresIn,
      },
    }),
    TypeOrmModule.forFeature([DatabaseUserRepository]),
  ],
  providers: [
    AuthenticationService,
    JwtStrategy,
    LocalStrategy,
    {
      inject: [DatabaseUserRepository, AuthenticationService],
      provide: Signin,
      useFactory: (
        databaseUserRepository: DatabaseUserRepository,
        authenticationService: AuthenticationService,
      ) => new Signin(databaseUserRepository, authenticationService),
    },
  ],
  exports: [AuthenticationService],
})
export class AuthenticationModule {}
