import { InvalidUserError } from './invalid-user.error';
import { UserPassword, UserUsername } from './type-aliases';

export class User {
  username: UserUsername;
  password: UserPassword;

  constructor(username: string, password: string) {
    if (!username || !password) {
      throw new InvalidUserError('username/password cannot be null or empty');
    }
    this.username = username;
    this.password = password;
  }
}
