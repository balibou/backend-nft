import { User } from './user';

export interface UserRepository {
  validateUserUsername(sub: any);
  signup(user: User): Promise<string>;
  validateUserPassword(user: User): Promise<string | null>;
}
