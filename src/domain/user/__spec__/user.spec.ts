import { InvalidUserError } from '../invalid-user.error';
import { User } from '../user';

describe('domain/User', () => {
  it('should throw an error when username is null', () => {
    const username = null;
    const password = 'xxx';
    const result = () => new User(username, password);

    expect(result).toThrow(
      new InvalidUserError('username/password cannot be null or empty'),
    );
  });
  it('should throw an error when username is empty', () => {
    const username = '';
    const password = 'xxx';
    const result = () => new User(username, password);

    expect(result).toThrow(
      new InvalidUserError('username/password cannot be null or empty'),
    );
  });
  it('should throw an error when password is null', () => {
    const username = 'user';
    const password = null;
    const result = () => new User(username, password);

    expect(result).toThrow(
      new InvalidUserError('username/password cannot be null or empty'),
    );
  });
  it('should throw an error when password is empty', () => {
    const username = 'user';
    const password = '';
    const result = () => new User(username, password);

    expect(result).toThrow(
      new InvalidUserError('username/password cannot be null or empty'),
    );
  });
  it('should create a user', () => {
    const username = 'user';
    const password = 'xxx';
    const result = new User(username, password);

    expect(result).toEqual({ username: 'user', password: 'xxx' });
  });
});
