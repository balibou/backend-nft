import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';
import { RestModule } from './infrastructure/rest/rest.module';

@Module({
  imports: [TypeOrmModule.forRoot(typeOrmConfig), RestModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
