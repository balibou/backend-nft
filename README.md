## Description

Backend for NFT

## Get started
### Install

```bash
$ yarn
```

### Database migrations

When running for the first time, or after database deletion, you have to run migrations to create a local database:
```
yarn run typeorm:migration:run
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```
