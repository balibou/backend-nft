import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as request from 'supertest';
import { typeOrmConfig } from '../src/config/typeorm.config';
import { RestModule } from '../src/infrastructure/rest/rest.module';
import { DatabaseUserRepository } from '../src/infrastructure/repositories/database-user.repository';

describe('infrastructure/rest/UserController (e2e)', () => {
  let app: INestApplication;
  let userRepository: DatabaseUserRepository;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RestModule, TypeOrmModule.forRoot(typeOrmConfig)],
    }).compile();
    app = moduleFixture.createNestApplication();
    await app.init();
    userRepository = await moduleFixture.get(DatabaseUserRepository);
  });

  beforeEach(async () => {
    await userRepository.clear();
  });

  describe('POST /api/user/signup', () => {
    it('should create user using value from body', async () => {
      const response: request.Response = await request(app.getHttpServer())
        .post('/api/user/signup')
        .send({ username: 'user', password: 'xxx' })
        .expect(201);

      expect(response.text).toEqual('user');
    });
    it('should throw an error when username is empty', async () => {
      const response: request.Response = await request(app.getHttpServer())
        .post('/api/user/signup')
        .send({ username: '', password: 'xxx' })
        .expect(400);

      expect(response.body.message).toEqual(
        'username/password cannot be null or empty',
      );
    });
    it('should throw an error when username is null', async () => {
      const response: request.Response = await request(app.getHttpServer())
        .post('/api/user/signup')
        .send({ username: null, password: 'xxx' })
        .expect(400);

      expect(response.body.message).toEqual(
        'username/password cannot be null or empty',
      );
    });
    it('should throw an error when password is empty', async () => {
      const response: request.Response = await request(app.getHttpServer())
        .post('/api/user/signup')
        .send({ username: 'user', password: '' })
        .expect(400);

      expect(response.body.message).toEqual(
        'username/password cannot be null or empty',
      );
    });
    it('should throw an error when password is null', async () => {
      const response: request.Response = await request(app.getHttpServer())
        .post('/api/user/signup')
        .send({ username: 'user', password: null })
        .expect(400);

      expect(response.body.message).toEqual(
        'username/password cannot be null or empty',
      );
    });
  });

  describe('POST /api/user/signin', () => {
    it('should signin a user using value from body', async () => {
      await userRepository.signup({ username: 'user', password: 'xxx' });
      const response: request.Response = await request(app.getHttpServer())
        .post('/api/user/signin')
        .send({ username: 'user', password: 'xxx' })
        .expect(200);

      expect(response.body).toEqual({ accessToken: expect.any(String) });
    });
    it('should throw an error when user does not exist', async () => {
      const response: request.Response = await request(app.getHttpServer())
        .post('/api/user/signin')
        .send({ username: 'user', password: 'xxx' })
        .expect(401);

      expect(response.body.message).toEqual('Invalid credentials');
    });
    it('should throw an error when user password does not match', async () => {
      await userRepository.signup({ username: 'user', password: 'xxx' });
      const response: request.Response = await request(app.getHttpServer())
        .post('/api/user/signin')
        .send({ username: 'user', password: 'yyy' })
        .expect(401);

      expect(response.body.message).toEqual('Invalid credentials');
    });

    it('should throw an error when username is empty', async () => {
      const response: request.Response = await request(app.getHttpServer())
        .post('/api/user/signin')
        .send({ username: '', password: 'xxx' })
        .expect(401);

      expect(response.body.message).toEqual('Unauthorized');
    });
    it('should throw an error when username is null', async () => {
      const response: request.Response = await request(app.getHttpServer())
        .post('/api/user/signin')
        .send({ username: null, password: 'xxx' })
        .expect(401);

      expect(response.body.message).toEqual('Unauthorized');
    });
    it('should throw an error when password is empty', async () => {
      const response: request.Response = await request(app.getHttpServer())
        .post('/api/user/signin')
        .send({ username: 'user', password: '' })
        .expect(401);

      expect(response.body.message).toEqual('Unauthorized');
    });
    it('should throw an error when password is null', async () => {
      const response: request.Response = await request(app.getHttpServer())
        .post('/api/user/signin')
        .send({ username: 'user', password: null })
        .expect(401);

      expect(response.body.message).toEqual('Unauthorized');
    });
  });
  describe('GET /api/user/me', () => {
    it('should get a user infos', async () => {
      await userRepository.signup({ username: 'user', password: 'xxx' });
      const responseOne: request.Response = await request(app.getHttpServer())
        .post('/api/user/signin')
        .send({ username: 'user', password: 'xxx' });

      const token = responseOne.body.accessToken;
      const response: request.Response = await request(app.getHttpServer())
        .get('/api/user/me')
        .set('Authorization', 'bearer ' + token)
        .expect(200);

      expect(response.text).toBe('user');
    });
    it('should throw an error when token is invalid', async () => {
      await userRepository.signup({ username: 'user', password: 'xxx' });
      const responseOne: request.Response = await request(app.getHttpServer())
        .post('/api/user/signin')
        .send({ username: 'user', password: 'xxx' });

      const token = responseOne.body.accessToken + 'e';
      const response: request.Response = await request(app.getHttpServer())
        .get('/api/user/me')
        .set('Authorization', 'bearer ' + token)
        .expect(401);

      expect(response.body.message).toEqual('Unauthorized');
    });
  });
});
