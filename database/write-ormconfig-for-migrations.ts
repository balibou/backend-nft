import * as fs from 'fs';
import { PathLike } from 'fs';
import * as path from 'path';
import { typeOrmConfig } from 'src/config/typeorm.config';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

const typeOrmConfigFilePath: PathLike = path.join(
  __dirname,
  '../ormconfig.json',
);
const typeOrmMigrationsOptions: TypeOrmModuleOptions = typeOrmConfig;
try {
  fs.unlinkSync(typeOrmConfigFilePath);
} catch (e) {
  // tslint:disable-next-line:no-console
  console.log(
    `Failed to delete file ${typeOrmConfigFilePath}. Probably because it does not exist.`,
  );
}
fs.writeFileSync(
  typeOrmConfigFilePath,
  JSON.stringify([typeOrmMigrationsOptions], null, 2),
);
